#!/bin/sh
_= # --[[
lua "$0"
exit
]] _G

-- Above is a "Lua-compatible" variant of the almighty shebang.
-- Make this file executable, and it'll likely run fine from most *nix shells.

-- Tested in Lua 5.4.3

-- Written for *nix, because that was easy enough.
-- Works under Linux, and most likely macOS and other *nixes.
-- No idea how to adapt it to Windows.

board = {{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}}
score = {0,0}
es = "\027["

function main()
    local brk = function() return true end
    local funcs = {}
    funcs['A'] = push_lt ; funcs['a'] = push_lt
    funcs['4'] = push_lt ; funcs['\27[D'] = push_lt
    funcs['W'] = push_up ; funcs['w'] = push_up
    funcs['8'] = push_up ; funcs['\27[A'] = push_up
    funcs['S'] = push_dn ; funcs['s'] = push_dn
    funcs['2'] = push_dn ; funcs['\27[B'] = push_dn
    funcs['D'] = push_rt ; funcs['d'] = push_rt
    funcs['6'] = push_rt ; funcs['\27[C'] = push_rt
    funcs['X'] = brk ; funcs['x'] = brk
    funcs['Q'] = brk ; funcs['q'] = brk
    funcs['\27[\27['] = brk ; funcs['\24'] = brk

    local run = true
    print_board()
    add_num()
    add_num()
    print_state()
    os.execute("stty cbreak </dev/tty >/dev/tty 2>&1")
    while run do
        k = io.read(1)
        io.write("\027[2K\027[10D")
        if k == "\027" then
            k = k .. io.read(1)
            io.write("\027[2K\027[10D")
            if string.sub(k,2) == "[" then
                k = k .. io.read(1)
                io.write("\027[2K\027[10D")
            else
                break
            end
        end
        --if not ( k == "" )
        --then
        --    break
        --end
        if funcs[k] then
            -- Functions return a falsey value unless otherwise specified.
            -- In order to not have to tell every function to return true, we
            -- just do it for "stop running" and invert the reply.
            run = not funcs[k]()
            print_state()
        end
    end
    io.write("\027[2K\027[10D")
    os.execute("stty -cbreak </dev/tty >/dev/tty 2>&1")
end

function print_board()
    print(es .. "2J")
    print(es .. '2;3HScore:')
    print(es .. '3;3HTurns:')
    print(es .. 2*#board+7 .. ';3HControls: ASWD, HJKL, 4286')
    print(es .. 2*#board+8 .. ';7HQuit: Q, X, Esc')
    for r = 1,#board,1 do
        p = {es .. r*2+3 .. ";5H",
             es .. r*2+4 .. ";5H"}
        print(p[1] .. string.rep("+" .. string.rep('-',4),#board[r]) .. "+")
        print(p[2] .. string.rep("|" .. string.rep(' ',4),#board[r]) .. "|")
    end
    print(es .. 2*#board+5 .. ';5H' .. string.rep("+" .. string.rep('-',4),#board[#board]) .. "+" )
end

function print_state()
    print(es .. '2;10H' .. score[1])
    print(es .. '3;10H' .. score[2])
    for r = 1,#board,1 do
        for c = 1,#board[r],1 do
            p = es .. 2*r+4 .. ";" .. 5*c+1 .. "H"
            print(p .. "    ")
            if board[r][c] > 0 then
                print(p .. board[r][c])
            end
        end
    end
    print(es .. 2*#board+9 .. ";0H")
end

function add_num()
    local zeroes = {}
    local num = 2
    local cell = 0
    if math.random(1,10) == 1 then
        num = 4
    end
    for r = 1,#board,1 do
        for c = 1,#board[r],1 do
            if board[r][c] == 0 then
                table.insert(zeroes,{r,c})
            end
        end
    end
    cell = math.random(1,#zeroes)
    board[zeroes[cell][1]][zeroes[cell][2]] = num
end

function mirror()
    local temp = {}
    for r = 1,#board,1 do
        temp = {}
        for c = 1,#board[r],1 do
            temp[c] = board[r][#board[r]+1-c]
        end
        board[r] = temp
    end
end

function invert()
    for r = 1,#board-1,1 do
        for c = r+1,#board[r],1 do
            temp = board[r][c]
            board[r][c] = board[c][r]
            board[c][r] = temp
        end
    end
end

function push_lt()
    local changed = false
    local tempc = 1
    for r = 1,#board,1 do
        tempc = 1
        for c = 2,#board[r],1 do
            if not (board[r][c] == 0) then
                while tempc < c do
                    if board[r][tempc] == 0 then
                        board[r][tempc] = board[r][c]
                        board[r][c] = 0
                        changed = true
                        break
                    elseif board[r][tempc] == board[r][c] then
                        board[r][tempc] = board[r][tempc]*2
                        score[1] = score[1] + board[r][tempc]
                        board[r][c] = 0
                        tempc = tempc + 1
                        changed = true
                        break
                    else
                        tempc = tempc + 1
                    end
                end
            end
        end
    end
    if changed then
        score[2] = score[2] + 1
        add_num()
    end
end

function push_rt()
    mirror()
    push_lt()
    mirror()
end

function push_up()
    invert()
    push_lt()
    invert()
end

function push_dn()
    invert()
    push_rt()
    invert()
end

main()
